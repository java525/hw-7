package ua.hillel.participant;

public class Robot extends Participant{

    private final String name;
    private final double maxRun;
    private final double maxJump;

    public Robot(String name, double maxRun, double maxJump) {
        this.name = name;
        this.maxRun = maxRun;
        this.maxJump = maxJump;
    }

    public void run(){
        System.out.println("Running");
    }

    public void jump(){
        System.out.println("Jumping");
    }

    public boolean run(Obstacle obstacle){
        System.out.println(this.getName() + " is running through " + obstacle.getName() + "...");
        if (maxRun > obstacle.getLength()) {
            return true;
        } else  {
            return false;
        }

    }

    public boolean jump(Obstacle obstacle){
        System.out.println(this.getName() + " is jumping over " + obstacle.getName() + "...");
        if (maxRun > obstacle.getLength()) {
            return true;
        } else  {
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public double getMaxRun() {
        return maxRun;
    }

    public double getMaxJump() {
        return maxJump;
    }

}