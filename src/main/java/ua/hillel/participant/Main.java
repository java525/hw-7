package ua.hillel.participant;

public class Main {
    public static void main(String[] args) {

        Human human = new Human("Human", 20, 2);
        Cat cat = new Cat("Cat", 50, 10);
        Robot robot = new Robot("Robot", 39, 100);

        Participant[] participants = {human, cat, robot};

        Obstacle treadmill = new Treadmill("treadmill", 40.00);
        Obstacle wall = new Wall("wall", 5.50);

        Obstacle[] obstacles = {treadmill, wall};

        for (int i = 0; i < participants.length; i++) {
            for (int j = 0; j < obstacles.length; j++) {
                if (!obstacles[j].overcome(participants[i])) {
                    break;
                }
            }
        }
    }
}