package ua.hillel.participant;

public class Wall extends Obstacle {

    private final String name;
    private final double height;

    public Wall(String name, double height) {
        this.name = name;
        this.height = height;
    }

    public boolean overcome(Participant participant) {
        if (participant.jump(this)) {
            System.out.println("Participant " + participant.getName() + " passed an obstacle " + this.getName() +
                    " on distance " + this.getHeight() + "m");
            return true;
        } else {
            System.out.println("Participant " + participant.getName() + " did not pass an obstacle " + this.getName() +
                    "on distance" + this.getLength() + "m" + ". Passed only " + participant.getMaxRun() + "m");
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public double getHeight() {
        return height;
    }
}