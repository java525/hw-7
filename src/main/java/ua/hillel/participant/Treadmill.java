package ua.hillel.participant;

public class Treadmill extends Obstacle {

    private final String name;
    private final double length;

    public Treadmill(String name, double length) {
        this.name = name;
        this.length = length;
    }

    public boolean overcome(Participant participant) {
        if (participant.run(this)) {
            System.out.println("Participant " + participant.getName() + " passed an obstacle " + this.getName() +
                    " on distance " + this.getLength() + "km");
            return true;
        } else {
            System.out.println("Participant " + participant.getName() + " did not pass an obstacle " + this.getName() +
                    " on distance " + this.getLength() + "km" + ". Passed only " + participant.getMaxRun() + "km");
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public double getLength() {
        return length;
    }
}