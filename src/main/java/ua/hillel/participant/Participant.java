package ua.hillel.participant;

public abstract class Participant {

    private String name;
    private double maxRun;
    private double maxJump;

    public void run() {
    }

    public void jump() {
    }

    public boolean run(Obstacle obstacle) {
        return false;
    }

    public boolean jump(Obstacle obstacle) {
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMaxRun() {
        return maxRun;
    }

    public void setMaxRun(double maxRun) {
        this.maxRun = maxRun;
    }

    public double getMaxJump() {
        return maxJump;
    }

    public void setMaxJump(double maxJump) {
        this.maxJump = maxJump;
    }
}