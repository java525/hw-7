package ua.hillel.participant;

public abstract class Obstacle {

    private String name;
    private double length;
    private double height;

    public boolean overcome(Participant participant) {
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}