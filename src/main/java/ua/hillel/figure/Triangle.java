package ua.hillel.figure;

public class Triangle implements Figure{

    private double base;
    private double heightToBase;

    public Triangle(double base, double heightToBase) {
        this.base = base;
        this.heightToBase = heightToBase;
    }

    @Override
    public double countArea() {
        double area = (base * heightToBase) / 2;

        return area;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeightToBase() {
        return heightToBase;
    }

    public void setHeightToBase(double heightToBase) {
        this.heightToBase = heightToBase;
    }
}
