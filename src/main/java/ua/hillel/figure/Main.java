package ua.hillel.figure;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(5);
        Square square = new Square(4);
        Triangle triangle = new Triangle(10, 5);
        Figure[] figures = {circle, square, triangle};

        System.out.println(sumOfAreas(figures));
    }

    private static double sumOfAreas(Figure[] figures) {
        double sum = 0;
        for (int i = 0; i < figures.length; i++) {
            sum += figures[i].countArea();
        }
        return sum;
    }
}

