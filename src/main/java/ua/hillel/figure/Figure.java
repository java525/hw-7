package ua.hillel.figure;

public interface Figure {

    default double countArea() {
        return 0;
    }
}
