package ua.hillel.figure;

public class Square implements Figure{

    private double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public double countArea() {
        double area = side * side;

        return area;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}
